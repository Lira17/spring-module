package io.steps.basics.task2;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

public class TaskBPP2 {

    interface ITarget {
        void method() throws InterruptedException;
    }

    @java.lang.annotation.Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @interface TimeAnnotation {

    }

    @Component
    static class Target implements ITarget {

        @Override
        @TimeAnnotation
        public void method() throws InterruptedException {
            System.out.println("My method invoked");
            TimeUnit.SECONDS.sleep(3);
        }
    }

    @Component
    static class TimeAnnotationBeanPostProcessor implements BeanPostProcessor {

        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

            for (Method declaredMethod : bean.getClass().getDeclaredMethods()) {
                TimeAnnotation annotation = declaredMethod.getAnnotation(TimeAnnotation.class);
                if (annotation == null) {
                    continue;
                }
                final var interfaces = bean.getClass().getInterfaces();
                return Proxy.newProxyInstance(bean.getClass().getClassLoader(), interfaces,
                        (proxy, method, args) -> {
                            System.out.println("Method started at " + LocalTime.now());
                            final var result = method.invoke(bean, args);
                            System.out.println("Method ended at " + LocalTime.now());
                            return result;
                        });
            }
            return bean;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        final var context = new AnnotationConfigApplicationContext(
                Target.class, TimeAnnotationBeanPostProcessor.class);

        final var obj = context.getBean(ITarget.class);
        obj.method();
    }
}