package io.steps.basics.task1;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskBPP1 {

    @Component
    static class Target {

        @ExampleAnnotation(name = "property.first")
        private String property;

        public String getProperty() {
            return property;
        }
    }

    @java.lang.annotation.Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    @interface ExampleAnnotation {
        String name();
    }

    @Component
    static class PropertyInjectingBeanPostProcessor implements BeanPostProcessor {

        private static final String PROPS_PATH = "/application.properties";

        private final Map<String, String> properties;

        public PropertyInjectingBeanPostProcessor() {
            try {
                final var path = Paths.get(TaskBPP1.class.getResource(PROPS_PATH).toURI());
                properties = Files
                        .readAllLines(path)
                        .stream()
                        .collect(Collectors.toMap(
                                line -> line.substring(0, line.indexOf('=')),
                                line -> line.substring(line.indexOf('=') + 1)
                        ));
            } catch (IOException | URISyntaxException e) {
                throw new IllegalStateException();
            }
        }

        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

            Stream.of(bean.getClass().getDeclaredFields())
                    .filter(f -> f.getAnnotation(ExampleAnnotation.class) != null)
                    .forEach(f -> {
                        final var name = f.getAnnotation(ExampleAnnotation.class).name();
                        final var value = Optional
                                .ofNullable(properties.get(name))
                                .orElseThrow(() -> new IllegalStateException("No such property: " + name));
                        try {
                            f.set(bean, value);
                        } catch (IllegalAccessException e) {
                            throw new IllegalStateException("Can't set filed '" + f.getName() + "'");
                        }
                    });

            return bean;
        }
    }

    public static void main(String[] args) {
        final var context = new AnnotationConfigApplicationContext(
                Target.class, PropertyInjectingBeanPostProcessor.class);

        final var obj = context.getBean(Target.class);
        System.out.println(obj.getProperty());
    }
}