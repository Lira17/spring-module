package io.examplespring.basics.entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Data
@Component
public class Bus {

    private final Passenger passenger;
    private final ApplicationContext applicationContext;

    @Autowired
    public Bus(Passenger passenger, ApplicationContext applicationContext) {
        this.passenger = passenger;
        this.applicationContext = applicationContext;
    }

    /*
    Это не имеет значения, потому что этот метод фактически будет динамически переопределен spring.
    Spring использует для этого библиотеку CGLIB.
     */
    @Lookup
    public Passenger createPassengers() {
        return null;
    }


    /*
    Заинжектили prototype passenger но ведет себя как будто он singleton
    Мы инжектим prototype во внутрь singleton и spring больше не имеет возможности подменить этот инстанст
     */
    public String getPhrase() {
        return "Hello, " + passenger.getName();
    }

    public String getPhraseLookup() {
        return "Hello, " + createPassengers().getName();
    }

    public String getPhraseByApplicationContext() {
        return "Hello, " + applicationContext.getBean(Passenger.class).getName();
    }
}
