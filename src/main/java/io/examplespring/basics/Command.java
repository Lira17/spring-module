package io.examplespring.basics;

import io.examplespring.basics.config.ApplicationConfig;
import io.examplespring.basics.entity.Bus;
import io.examplespring.basics.entity.Passenger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.util.stream.IntStream;

@ShellComponent
public class Command {

    final ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class, Bus.class);

    @ShellMethod("Get passengers")
    public void get_passengers() {
        IntStream
                .range(0, 3)
                .mapToObj(i -> context.getBean(Passenger.class))
                .forEachOrdered(System.out::println);
    }

    @ShellMethod("Get buses without prototype.")
    public void get_buses_without_prototype() {
        IntStream
                .range(0, 3)
                .mapToObj(i -> context.getBean(Bus.class).getPhrase())
                .forEachOrdered(System.out::println);

    }

    @ShellMethod("Get all buses.")
    public void get_buses() {
        IntStream
                .range(0, 3)
                .mapToObj(i -> context.getBean(Bus.class).getPhraseLookup())
                .forEachOrdered(System.out::println);

    }

    @ShellMethod("Get all buses.")
    public void all_buses_without_annotation() {
        IntStream
                .range(0, 3)
                .mapToObj(i -> context.getBean(Bus.class).getPhraseByApplicationContext())
                .forEachOrdered(System.out::println);
    }
}

